//
//  MainViewController.swift
//  FyberChallenge
//
//  Created by Cheolhyun Wang on 11/8/17.
//  Copyright © 2017 Cheolhyun Wang. All rights reserved.
//

import UIKit

extension String {
    func sha1() -> String {
        let data = self.data(using: String.Encoding.utf8)!
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0, CC_LONG(data.count), &digest)
        }
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
}

class MainViewController: UIViewController {

    @IBOutlet weak var tfUID: UITextField!
    @IBOutlet weak var tfApiKey: UITextField!
    @IBOutlet weak var tfAppID: UITextField!
    @IBOutlet weak var tfPub0: UITextField!
    @IBOutlet weak var lblMesg: UILabel!
    
    var timestamp : Int?
    var systemVersion : String?
    var ip : String?
    var session: DHURLSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tfUID.text = "spiderman"
        self.tfApiKey.text = "1c915e3b5d42d05136185030892fbb846c278927"
        self.tfAppID.text = "2070"
        self.tfPub0.text = "campaign2"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Fyber Request
    
    func sortArray(arrayToSort: [String])->[String] {
        let sortedArray = arrayToSort.sorted(by:) {
            (first, second) in
            first.compare(second, options: .numeric) == ComparisonResult.orderedAscending
        }
        //print(sortedArray)
        return sortedArray
    }
    
    @IBAction func clickSend(_ sender: Any) {
        
        self.view.endEditing(true)
        
        self.lblMesg.text = ""
    
        let appid = self.tfAppID.text   //"2070"
        let uid = self.tfUID.text       //"spiderman"
        let apple_idfa = CommonFunction().getAvertisingIdentifier_uuid()
        let apple_idfa_tracking_enabled = CommonFunction().isAdvertisingTrackingEnabled()
        let locale = CommonFunction().getLocale() //"en_AU"
        let systemVersion = CommonFunction().getSystemVersion()
        
        let timeStamp = CommonFunction().getTimeStamp()
        let device = CommonFunction().getDeviceType()
        let ip = getIPAddresses()
        let offer_types = "112"         //,103,101
        let phone_version = CommonFunction().getDeviceVersion() //"iphone"
        let pub0 = self.tfPub0.text     //"campaign2"
        
        
        let dicParam = NSMutableDictionary()
        dicParam["appid"] = appid!
        dicParam["apple_idfa"] = apple_idfa
        if apple_idfa_tracking_enabled {
            dicParam["apple_idfa_tracking_enabled"] = "true"
        } else {
            dicParam["apple_idfa_tracking_enabled"] = "false"
        }
        dicParam["device"] = device
        dicParam["ip"] = "109.235.143.113" //ip!
        dicParam["locale"] = locale
        dicParam["offer_types"] = offer_types
        dicParam["os_version"] = systemVersion
        dicParam["phone_version"] = phone_version
        dicParam["pub0"] = pub0!
        dicParam["timestamp"] = String(timeStamp)
        dicParam["uid"] = uid!
        
//        dicParam["page"] = "1"
//        dicParam["ps_time"] = String(timeStamp)
//        dicParam["pub1"] = pub0!
        
        var param : String?
        
        let keys = dicParam.allKeys
        let sortedKeys = sortArray(arrayToSort: keys as! [String])
        for key in sortedKeys {
            //print("key=\(key)")
            let value = dicParam[key]
            if param == nil {
                param = key + "=" + (value as? String)!
            } else {
                param = param!  + "&" + key + "=" + (value as? String)!
            }
        }
        //print("param = \(param!)")
        
        
        let api_key = self.tfApiKey.text
        let paramWithApiKey = "\(param!)&\(api_key!)"
        let hash = paramWithApiKey.sha1()
        //print("hash = \(hash)")
        param = param! + "&hashkey=" + hash
        
        let strURL: String = "http://api.fyber.com/feed/v1/offers.json?\(param!)"
        //print("strURL = \(strURL)")

        guard let url = NSURL(string: strURL) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url as URL)
        
        let config = URLSessionConfiguration.default
        session = URLSession(configuration: config)
        
        let task = session?.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            //print("response = \(response!)")
            //print("data = \(data!)")
            
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error!)
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    self.updateSearchResults(responseData)
                }
            }
        })
        task?.resume()
    }
    
    func updateSearchResults(_ responseData: Data?) {
        
        do {
            guard let response_data = try JSONSerialization.jsonObject(with: responseData!, options: []) as? [String: AnyObject] else {
                print("error trying to convert data to JSON")
                return
            }
            //print("response_data: " + response_data.description)
            
            guard let message = response_data["message"] as? String else {
                print("Could not get message from JSON")
                return
            }
            //print("message: " + message)
            
            guard let dicInfo = response_data["information"] as? NSDictionary else {
                print("Could not get information from JSON")
                return
            }
            //print("app_name: " + (dicInfo["app_name"] as! String))
            
            
            guard let arrayOffers = response_data["offers"] as? NSArray else {
                print("Could not get offers from JSON")
                return
            }
            DataManager.sharedManager.offers.value = arrayOffers as! [NSDictionary]
            //print("arrayOffers.count = \(arrayOffers.count)")
            
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    self.title = dicInfo["app_name"] as? String
                    self.lblMesg.text = message
                    
                    let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "OffersListVC") as? OffersListViewController
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
            
            guard let count = response_data["count"] as? Int else {
                print("Could not get count from JSON")
                return
            }
            //print("count = \(count)")
            
        } catch  {
            print("error trying to convert data to JSON")
            return
        }
    }
    
    // MARK: - IPAddress Functions
    
    func getWiFiAddress() -> String? {
        var address : String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if  name == "en0" {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        
        return address
    }
    
    func getIPAddresses() -> String? {
        var addresses = [String]()
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return "" }
        guard let firstAddr = ifaddr else { return "" }
        
        // For each interface ...
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let flags = Int32(ptr.pointee.ifa_flags)
            let addr = ptr.pointee.ifa_addr.pointee
            
            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                        let address = String(cString: hostname)
                        addresses.append(address)
                    }
                }
            }
        }
        
        freeifaddrs(ifaddr)
        
        for i in 0..<addresses.count {
            let ip = addresses[0] as String
            //print("ip=\(ip)")
            if ip.contains(".") {
                return addresses[i]
            }
        }
        
        return getWiFiAddress()
    }

}

