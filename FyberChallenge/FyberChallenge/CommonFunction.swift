//
//  CommonFunction.swift
//  FyberChallenge
//
//  Created by Cheolhyun Wang on 12/8/17.
//  Copyright © 2017 Cheolhyun Wang. All rights reserved.
//

import UIKit
import AdSupport

class CommonFunction: NSObject {
    
    func getTimeStamp() -> Int {
        return Int(Date().timeIntervalSince1970)
    }
    
    func getSystemVersion() -> String {
        return UIDevice.current.systemVersion
    }
    
    func getDeviceVersion() -> String {
        return UIDevice.current.model
    }
    
    func getAvertisingIdentifier_uuid() -> String {
        //print("advertisingIdentifier_uuid = \(ASIdentifierManager.shared().advertisingIdentifier.uuidString)")
        return ASIdentifierManager.shared().advertisingIdentifier.uuidString
    }
    
    func isAdvertisingTrackingEnabled() -> Bool {
        //print("isAdvertisingTrackingEnabled = \(ASIdentifierManager.shared().isAdvertisingTrackingEnabled)")
        return ASIdentifierManager.shared().isAdvertisingTrackingEnabled
    }
    
    func getDeviceType() -> String {
        return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad ? "tablet" : "phone"
    }
    
    func getLocale() -> String {
        let locale = Locale.preferredLanguages[0]
        //print("locale = \(locale)")
        return locale
    }
    
}
