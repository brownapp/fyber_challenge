//
//  OfferViewController.swift
//  FyberChallenge
//
//  Created by Cheolhyun Wang on 13/8/17.
//  Copyright © 2017 Cheolhyun Wang. All rights reserved.
//

import UIKit

class OfferViewController: UIViewController {

    @IBOutlet weak var imgHires: UIImageView!
    @IBOutlet weak var imgLowres: UIImageView!
    @IBOutlet weak var lblTeaser: UILabel!
    @IBOutlet weak var lblLink: UILabel!
    
    @IBOutlet weak var vwTeaser: UIView!
    @IBOutlet weak var vwLink: UIView!
    
    var offer : NSDictionary? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.vwTeaser.clipsToBounds = true
        self.vwTeaser.layer.cornerRadius = 5
        self.vwTeaser.layer.borderColor = UIColor.gray.cgColor
        self.vwTeaser.layer.borderWidth = 1
        
        self.vwLink.clipsToBounds = true
        self.vwLink.layer.cornerRadius = 5
        self.vwLink.layer.borderColor = UIColor.gray.cgColor
        self.vwLink.layer.borderWidth = 1
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        let arrayImage = self.offer!["thumbnail"] as! NSDictionary
        
        let hires = arrayImage["hires"] as? String
        //print("hires=\(hires!)")
        
        if let strImage = hires {
            let url = URL(string:strImage)
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    self.imgHires.image = UIImage(data: data!)
                }
                self.imgHires.isHidden = false
            }
        } else {
            self.imgHires.image = UIImage.init(named: "default")
        }
        
        let lowres = arrayImage["lowres"] as? String
        //print("lowres=\(lowres!)")
        
        if let strImage = lowres {
            let url = URL(string:strImage)
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    self.imgLowres.image = UIImage(data: data!)
                }
                self.imgLowres.isHidden = false
            }
        } else {
            self.imgLowres.image = UIImage.init(named: "default")
        }
        
        
        var string_1 = "Teaser : "
        var string_2 = self.offer!["teaser"] as? String
        
        var attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: string_1,
        attributes: [NSForegroundColorAttributeName: UIColor.black]))
        attributedString.append(NSAttributedString(string: string_2!,
        attributes: [NSForegroundColorAttributeName: UIColor.darkGray]))
        
        attributedString.addAttribute(NSFontAttributeName,value: UIFont(name: "Helvetica-Bold", size: 17.0)!,
        range: NSRange(location: 0, length: string_1.characters.count))
 
        self.lblTeaser.attributedText = attributedString
        
        
        
        
        string_1 = "Link : "
        string_2 = self.offer!["link"] as! String
        
        attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: string_1,
                                                   attributes: [NSForegroundColorAttributeName: UIColor.black]))
        attributedString.append(NSAttributedString(string: string_2!,
                                                   attributes: [NSForegroundColorAttributeName: UIColor.darkGray]))
        
        attributedString.addAttribute(NSFontAttributeName,value: UIFont(name: "Helvetica-Bold", size: 17.0)!,
                                      range: NSRange(location: 0, length: string_1.characters.count))
        

        self.lblLink.attributedText = attributedString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
