import UIKit

class OfferCell: UITableViewCell {
  
    static let Identifier = "OfferCell"
  
    @IBOutlet private var lblTitle: UILabel!
    @IBOutlet private var lblOfferID: UILabel!
    @IBOutlet private var imgThumbnail: UIImageView!
  
    func configureWithOffer(dictionary: NSDictionary) {
        lblTitle.text = dictionary["title"] as? String
        lblOfferID.text = String(describing:dictionary["offer_id"]!)
        
        let arrayImage = dictionary["thumbnail"] as! NSDictionary
        let lowres = arrayImage["lowres"] as? String
        //print("lowres=\(lowres!)")
        
        if let strImage = lowres {
            let url = URL(string:strImage)
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    self.imgThumbnail.image = UIImage(data: data!)
                }
                self.imgThumbnail.isHidden = false
            }
        } else {
            self.imgThumbnail.image = UIImage.init(named: "default")
        }
    }
}
