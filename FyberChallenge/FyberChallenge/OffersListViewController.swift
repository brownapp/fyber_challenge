//
//  OffersListViewController.swift
//  FyberChallenge
//
//  Created by Cheolhyun Wang on 13/8/17.
//  Copyright © 2017 Cheolhyun Wang. All rights reserved.
//

import UIKit
import RxSwift

class OffersListViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet private var tableViewOffers: UITableView!
    @IBOutlet weak var lblNoOffers: UILabel!
    
    let arrayOffers : NSArray? = nil
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        setupOffersObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Functions
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: Rx Setup
    
    private func setupOffersObserver() {
        
        DataManager.sharedManager.offers.asObservable()
            .subscribe(onNext: {
                offers in
                
                if DataManager.sharedManager.offers.value.count > 0 {
                    self.tableViewOffers.isHidden = false
                    self.lblNoOffers.isHidden = true
                    self.tableViewOffers.reloadData()
                } else {
                    self.tableViewOffers.isHidden = true
                    self.lblNoOffers.isHidden = false
                }
            })
            .addDisposableTo(disposeBag)
    }
}

// MARK: - Table view data source

extension OffersListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.sharedManager.offers.value.count
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: OfferCell.Identifier, for: indexPath) as? OfferCell else {
            return UITableViewCell()
        }
        
        let offer = DataManager.sharedManager.offers.value[indexPath.row]
        cell.configureWithOffer(dictionary: offer)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
}

// MARK: - Table view delegate

extension OffersListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let offer = DataManager.sharedManager.offers.value[indexPath.row]
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyBoard.instantiateViewController(withIdentifier: "OfferVC") as? OfferViewController
        vc!.offer = offer
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
