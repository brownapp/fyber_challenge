//
//  FyberChallengeTests.swift
//  FyberChallengeTests
//
//  Created by Cheolhyun Wang on 11/8/17.
//  Copyright © 2017 Cheolhyun Wang. All rights reserved.
//

import XCTest
@testable import FyberChallenge

class FyberChallengeTests: XCTestCase {
    
    var commonFunction: CommonFunction!
    var mainViewController: MainViewController!
    
    override func setUp() {
        super.setUp()
        commonFunction = CommonFunction()
    
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        mainViewController = storyBoard.instantiateViewController(withIdentifier: "MainVC") as? MainViewController
        
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "offersData", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
        
        let url = URL(string: "http://api.fyber.com/feed/v1/offers.json?appid=2070&apple_idfa=037DA552-B036-4FC3-81EE-255CAE625F83&apple_idfa_tracking_enabled=true&device=phone&ip=109.235.143.113&locale=en&offer_types=112&os_version=10.3&phone_version=iPhone&pub0=campaign2&timestamp=1502669348&uid=spiderman&hashkey=8f98ba59d6a9e0baca9712091c71695ae75b8")
        let urlResponse = HTTPURLResponse(url: url!, statusCode: 200, httpVersion: nil, headerFields: nil)
        
        let sessionMock = URLSessionMock(data: data, response: urlResponse, error: nil)
        
        mainViewController.session = sessionMock as? DHURLSession
    }
    
    override func tearDown() {
        commonFunction = nil
        mainViewController = nil
        super.tearDown()
    }
    
    func testAdvertisingTracking() {
        let isEnabled = commonFunction.isAdvertisingTrackingEnabled()
        XCTAssertTrue(isEnabled)
    }
    
    func testUpdateOffersData() {
        // given
        let promise = expectation(description: "Status code: 200")
        
        // when
        XCTAssertEqual(DataManager.sharedManager.offers.value.count, 0, "offers should be empty before the data task runs")
        let url = URL(string: "http://api.fyber.com/feed/v1/offers.json?appid=2070&apple_idfa=037DA552-B036-4FC3-81EE-255CAE625F83&apple_idfa_tracking_enabled=true&device=phone&ip=109.235.143.113&locale=en&offer_types=112&os_version=10.3&phone_version=iPhone&pub0=campaign2&timestamp=1502669348&uid=spiderman&hashkey=8f98ba59d6a9e0baca9712091c71695ae75b8b17")
        let dataTask = mainViewController?.session?.dataTask(with: url!) {
            data, response, error in
            // if HTTP request is successful, call updateSearchResults(_:) which parses the response data into Tracks
            if let error = error {
                print(error.localizedDescription)
            } else if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    promise.fulfill()
                    self.mainViewController?.updateSearchResults(data)
                }
            }
        }
        dataTask?.resume()
        waitForExpectations(timeout: 5, handler: nil)
        
        // then
        XCTAssertEqual(DataManager.sharedManager.offers.value.count, 4, "Didn't parse 4 items from fake response")
    }
    
}
